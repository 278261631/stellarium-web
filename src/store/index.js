// Stellarium Web - Copyright (c) 2018 - Noctua Software Ltd
//
// This program is licensed under the terms of the GNU AGPL v3, or
// alternatively under a commercial licence.
//
// The terms of the AGPL v3 license can be found in the main directory of this
// repository.

import Vue from 'vue'
import Vuex from 'vuex'
import _ from 'lodash'

Vue.use(Vuex)

const createStore = () => {
  var pluginsModules = {}
  for (let i in Vue.SWPlugins) {
    let plugin = Vue.SWPlugins[i]
    if (plugin.storeModule) {
      pluginsModules[plugin.name] = plugin.storeModule
    }
  }

  return new Vuex.Store({
    modules: {
      plugins: {
        modules: pluginsModules
      }
    },
    state: {
      stel: null,
      initComplete: false,

      noctuaSky: {},

      showNavigationDrawer: false,
      showAboutDialog: false,
      showDataCreditsDialog: false,
      showPrivacyDialog: false,
      showViewSettingsDialog: false,
      showPlanetsVisibilityDialog: false,
      showLocationDialog: false,
      selectedObject: undefined,
      showScopeStatusDialog: false,
      showShootDialog: false,

      taskTargetName:'-',
      taskTargetRA:0,
      taskTargetDec:0,
      taskFilterList:[],

      showSidePanel: false,
      fullscreen: false,
      nightmode: false,
      wasmSupport: true,

      orange: false,

      selectedLocation: '高崖子',
      selectedTelescope: 'simulator',
      selectedTelesFilters:[{ bin:1,count:1,filterName: 'Null',expTime: 15,useable:true}
          , {bin:1,count:1,filterName: 'L',filterKey: 'L',expTime: 10,useable:false}
          , {bin:1,count:1,filterName: 'Luminance',filterKey: 'Luminance',expTime: 10,useable:false}
          , {bin:1,count:1,filterName: 'G',filterKey: 'G',expTime: 10,useable:false}
          , {bin:1,count:1,filterName: 'Green',filterKey: 'Green',expTime: 10,useable:false}
          , {bin:1,count:1,filterName: 'R',filterKey: 'R',expTime: 10,useable:false}
          , {bin:1,count:1,filterName: 'Red',filterKey: 'Red',expTime: 10,useable:false}
          , {bin:1,count:1,filterName: 'B',filterKey: 'B',expTime: 10,useable:false}
          , {bin:1,count:1,filterName: 'Blue',filterKey: 'Blue',expTime: 10,useable:false}
          , {bin:1,count:1,filterName: 'Ha',filterKey: 'Ha',expTime: 10,useable:false}
          , {bin:1,count:1,filterName: 'SII',filterKey: 'SII',expTime: 10,useable:false}
          , {bin:1,count:1,filterName: 'OIII',filterKey: 'OIII',expTime: 10,useable:false}
          , {bin:1,count:1,filterName: 'U',filterKey: 'Uk',expTime: 10,useable:false}
          , {bin:1,count:1,filterName: 'V',filterKey: 'Vk',expTime: 10,useable:false}
          , {bin:1,count:1,filterName: 'B',filterKey: 'Bk',expTime: 10,useable:false}
          , {bin:1,count:1,filterName: 'C',filterKey: 'Ck',expTime: 10,useable:false}
          , {bin:1,count:1,filterName: 'R',filterKey: 'Rk',expTime: 10,useable:false}
          , {bin:1,count:1,filterName: 'I',filterKey: 'Ik',expTime: 10,useable:false}
          , {bin:1,count:1,filterName: 'u',filterKey: 'uk',expTime: 10,useable:false}
          , {bin:1,count:1,filterName: 'v',filterKey: 'vk',expTime: 10,useable:false}
          , {bin:1,count:1,filterName: 'g',filterKey: 'gk',expTime: 10,useable:false}
          , {bin:1,count:1,filterName: 'r',filterKey: 'rk',expTime: 10,useable:false}
          , {bin:1,count:1,filterName: 'i',filterKey: 'ik',expTime: 10,useable:false}
          , {bin:1,count:1,filterName: 'z',filterKey: 'zk',expTime: 10,useable:false}
          ],
      activities: [
      // {content: '--',timestamp: '2018-04-15'},{content: '22',timestamp: '2018-04-13'}
        ],
      currentTaskName   :'',
      currentTask   :{content: '',disabled:true,ossPath:[]},

      autoDetectedLocation: {
        shortName: 'Unknown',
        country: 'Unknown',
        streetAddress: '',
        lat: 0,
        lng: 0,
        alt: 0,
        accuracy: 5000
      },

      currentLocation: {
        shortName: 'Unknown',
        country: 'Unknown',
        streetAddress: '',
        lat: 0,
        lng: 0,
        alt: 0,
        accuracy: 5000
      },

      useAutoLocation: true
    },
    mutations: {
      replaceStelWebEngine (state, newTree) {
        // mutate StelWebEngine state
        state.stel = newTree
      },
      replaceNoctuaSkyState (state, newTree) {
        // mutate NoctuaSky state
        state.noctuaSky = newTree
      },
      toggleBool (state, varName) {
        _.set(state, varName, !_.get(state, varName))
      },
      setValue (state, { varName, newValue }) {
        _.set(state, varName, newValue)
      },
      setAutoDetectedLocation (state, newValue) {
        state.autoDetectedLocation = { ...newValue }
        if (state.useAutoLocation) {
          state.currentLocation = { ...newValue }
        }
      },
      setUseAutoLocation (state, newValue) {
        state.useAutoLocation = newValue
        if (newValue) {
          state.currentLocation = { ...state.autoDetectedLocation }
        }
      },
      setCurrentLocation (state, newValue) {
        state.useAutoLocation = false
        state.currentLocation = { ...newValue }
      },
      setSelectedObject (state, newValue) {
        state.selectedObject = newValue
      },
      setTaskTargetRA (state, newValue) {
        state.taskTargetRA = newValue
      },
      setTaskTargetDec (state, newValue) {
        state.taskTargetDec = newValue
      }
    }
  })
}

export default createStore
